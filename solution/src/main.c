#include <stdio.h>

#include "format/bmp.h"
#include "file/fileio.h"
#include "transformation/transform_rotate.h"

int main(int argc, char** argv) {
    if (argc != 3) {
        fprintf(stderr, "Usage: ./bmp_rotator <source_file> <destination_file>");
        return 1;
    }

    FILE *in = NULL;
    FILE *out = NULL;

    struct image* new_image = NULL;
    if (!open_file(argv[1], &in, FILE_RB)) {
        perror("Cant open input file");
        return 1;
    }
    struct image *image = NULL;
    if (from_bmp(in, &image) == READ_OK) {
        new_image = rotate(*image);
    }
    image_destroy(image);
    if (!close_file(&in)) {
        perror("Cant close input file");
        return 1;
    }


    if (!open_file(argv[2], &out, FILE_WB)) {
        perror("Cant open output file");
        return 1;
    }

    to_bmp(out, new_image);
    image_destroy(new_image);
    if (!close_file(&out)) {
        perror("Cant close output file");
        return 1;
    }


    return 0;
}

    

