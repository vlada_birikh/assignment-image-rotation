#include "fileio.h"

bool open_file (const char* filename, FILE** file, const char* mode) {
    if (!filename) return false;
    *file = fopen( filename, mode );
    if (!*file) return false;

    return true;
}

bool close_file (FILE** file) {
    if (*file) {
        if (fclose(*file)) {
            return false;
        }
        return true;
    }
    return false;
}
