#ifndef BMP_IMAGE_ROTATOR_BMP_H
#define BMP_IMAGE_ROTATOR_BMP_H

#include "../image/image.h"
#include "status.h"
#include "stdint.h"
#include "stdio.h"

struct __attribute__((packed)) bmp_header{
    uint16_t bfSignature;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    int32_t biWidth;
    int32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static inline bool bmp_check_signature (const struct bmp_header* header);
static inline bool bmp_to_image (FILE* file, const struct bmp_header* header, struct image** img);
static inline struct bmp_header bmp_construct_header (struct image const* img);

enum read_status from_bmp (FILE* in, struct image** img);
enum write_status to_bmp (FILE* out, struct image const* img);

#endif //BMP_IMAGE_ROTATOR_BMP_H

