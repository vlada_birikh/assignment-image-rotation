#include "transform_rotate.h"
#include <stdio.h>

struct image * rotate(struct image const source ) {
    struct image* new_image = image_init(source.height, source.width);
    if (new_image == NULL)
    {
        return new_image;
    }

    int32_t j = source.width - 1;
    int32_t i = 0;
    for (i=0; i <= source.width - 1; i++) {
        for (int32_t k = 0; k < source.height; ++k) {
            struct pixel pixel = {0};
            image_get_pixel_by_coordinates(&source, k, j, &pixel);
                if (image_get_pixel_by_coordinates(&source, k, j, &pixel) != IMAGE_OK)
                {
                    return NULL;
                }
            array_array_pixel_set(*new_image->data, i, k, pixel);
        }

        if (j == 0 || i == source.width - 1) {
            break;
        }
        j--;
    }

    return new_image;
}
