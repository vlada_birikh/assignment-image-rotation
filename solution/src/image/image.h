#ifndef BMP_IMAGE_ROTATOR_IMAGE_H
#define BMP_IMAGE_ROTATOR_IMAGE_H

#include "../data_structures/marray_pixel.h"
#include "pixel.h"
#include "stdint.h"

struct image {
    int32_t width;
    int32_t height;
    struct array_array_pixel* data;
};

enum image_status {
    IMAGE_OK = 0,
    IMAGE_INVALID_COORDINATES,
};

struct image* image_init (int32_t width, int32_t height);
enum image_status image_get_pixel_by_coordinates (struct image const* image, int32_t x, int32_t y, struct pixel* pixel);
enum image_status image_set_pixel_by_coordinates (struct image const* image, int32_t x, int32_t y, struct pixel* pixel);
void image_destroy (struct image* image);

#endif //BMP_IMAGE_ROTATOR_IMAGE_H
